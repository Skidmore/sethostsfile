Takes the ExternalAddress parameter and does a DNS lookup to resolve the IP address.
Then creates a hosts file entry for the InternalAddress using the IP address obtained above.