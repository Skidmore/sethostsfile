﻿<#

.SYNOPSIS
    Sets a hosts file entry for a remote internal address to a looked up external address

.DESCRIPTION
    Takes the ExternalAddress parameter and does a DNS lookup to resolve the IP address.
    Then creates a hosts file entry for the InternalAddress using the IP address obtained above.

.EXAMPLE
    Create a host file entry using the public IP address endpoint of externalendpoint.westus.cloudapp.azure.com for the internal name of myapp.internal.cloudapp.net

    .\Set-HosFileEntry.ps1 -ExternalAddress externalendpoint.westus.cloudapp.azure.com -InternalAddress myapp.internal.cloudapp.net

.NOTES
    Author: Tony Skidmore <anthony.skidmore@accenture.com>
    Date: 22nd Feb 2016
    Version: 0.0.1

#>

param (
    [parameter(Mandatory = $true,
    Position = 0)]
    [string]
    $ExternalAddress,

    [parameter(Mandatory = $true,
    Position = 1)]
    [string]
    $InternalAddress
)

BEGIN
{

    # functions

    function Get-DnsIpAddress
    {
        [CmdletBinding()]
        param (
            [parameter(Mandatory = $true,
            Position = 0,
            ValueFromPipeline =$true)]
            [string]
            $DnsAddress
        )

        try
        {
            ([System.Net.Dns]::GetHostAddresses($DnsAddress)).IPAddressToString
        }
        catch [System.Management.Automation.MethodInvocationException]
        {
            if($_.Exception -match "No such host")
            {
                Write-Verbose "Host Not found"
            }
            else
            {
                Write-Verbose "[System.Management.Automation.MethodInvocationException] $($_.Exception.Message)"
            }
            return $null
        }
        catch
        {
            Write-Verbose "$($_.Exception.Message)"
            return $null
        }
    }


    function Add-HostFileEntry 
    {            
        [CmdletBinding(SupportsShouldProcess=$true)]            
        param (            
            [parameter(Mandatory=$true)]            
            [ValidatePattern("\b\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\b")]            
            [string]$IPAddress,            
              
            [parameter(Mandatory=$true)]            
            [string]$Computer            
        )  
              
        $file = Join-Path -Path $($env:windir) -ChildPath "\system32\drivers\etc\hosts"

        if (-not (Test-Path -Path $file)){            
            Throw "Hosts file not found"            
        }

        try
        {         
            $data = Get-Content -Path $file -ErrorAction Stop
            $data += "$IPAddress  $Computer"

            $data
           
            Set-Content -Value $data -Path $file -Force -Encoding ASCII -ErrorAction Stop
        }
        catch
        {
            Write-Error "$($_.Exception.Message)"
            return "Error"
        }
                    
    }

    function Remove-HostFileEntry {            
     [CmdletBinding()]            
        param (            
            [parameter(Mandatory=$true)]            
            [string]$Computer            
        ) 
                
        $file = Join-Path -Path $($env:windir) -ChildPath "system32\drivers\etc\hosts"            

        if (-not (Test-Path -Path $file)){            
            Throw "Hosts file not found"            
        }            

        $Content = Get-Content -Path $file

        if($Content -match $Computer)
        {
            $SavedErrPref = $ErrorActionPreference
            $ErrorActionPreference = "Stop"

            try
            {
                if(-not (Test-Path "$file-original-backup.txt")) { Copy-Item -Path $file -Destination "$file-original-backup.txt" -Force }
                Copy-Item -Path $file -Destination "$file-latest-backup.txt" -Force
                $data = ((Get-Content -Path $file) -notmatch $Computer)
                if($data.length -gt 0) { Set-Content -Value $data -Path $file -Force -Encoding ASCII }
            }
            catch
            {
                Write-Error "$($_.Exception.Message)"
                return "Error"
            }
            finally
            {
                $ErrorActionPreference = $SavedErrPref
            }
        }
    }
}

PROCESS
{

    $IPAddress = Get-DNSIPAddress -DnsAddress $ExternalAddress
    if($IPAddress)
    {
        $Result = Remove-HostFileEntry -Computer $InternalAddress
        if($Result -ne "Error") { Add-HostFileEntry -IPAddress $IPAddress -Computer $InternalAddress }
    }
    else
    {
        Write-Output "Unable to resolved IP address of $ExternalAddress"
    }

}

END {}
